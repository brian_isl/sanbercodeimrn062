// SOAL NOMOR 1
// LOOPING PERTAMA
var x = "I Love Coding"
var x1 = 1
console.log("\nLOOPING Pertama")
while (x1 <= 20) {
    if (x1 % 2 == 0) {
        console.log(x1 + " - " + x)
    }
    x1++

}

// LOOPING KEDUA
var y = "I will become a mobile developer"
var y1 = 20
console.log("\nLOOPING Kedua")
while (y1 >= 1) {
    if (y1 % 2 == 0) {
        console.log(y1 + " - " + y)
    }
    y1--

}

// SOAL NOMOR 2
var a = "Santai"
var b = "Berkualitas"
var c = "I Love Coding"
console.log("\nOUTPUT SANTAI BERKUALITAS I LOVE CODING")
for (let index = 1; index <= 20; index++) {
    if (index % 2 == 0) {
        console.log(index + " - " + b)
    } else if (index % 2 == 1) {
        if (index % 3 == 0) {
            console.log(index + " - " + c)
        } else {
            console.log(index + " - " + a)
        }
    }
}

// SOAL NOMOR 3
var e = "#"
console.log("\nOUTPUT PERSEGI")
for (var index0 = 0; index0 < 4; index0++) {
    var d = ""
    for (var index1 = 0; index1 < 8; index1++) {
        d += e;
    }
    console.log(d);
}

// SOAL NOMOR 4
var f = 7
var g = "#"

console.log("\nOUTPUT TANGGA")
for (var index2 = 1; index2 <= f; index2++) {
    var h = "\n";
    for (var index3 = 1; index3 <= index2; index3++) {

        h += g;
    }
    console.log(h);
}

// SOAL NOMOR 5
var k = "#"
var m = " "
console.log("\nOUTPUT PERSEGI")
for (var index4 = 0; index4 < 8; index4++) {
    var l = "\n"
    if (index4 % 2 == 1) {
        for (var index5 = 0; index5 < 8; index5++) {
            if (index5 % 2 == 0) {
                l += k;
            } else {
                l += m
            }
        }
    } else if (index4 % 2 == 0) {
        for (var index5 = 0; index5 < 8; index5++) {
            if (index5 % 2 == 0) {
                l += m;
            } else {
                l += k
            }
        }
    }
    console.log(l)
}