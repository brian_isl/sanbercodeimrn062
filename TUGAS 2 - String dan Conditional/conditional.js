// SOAL NOMOR 1
var nama = "Junaedi"
var peran = "Werewolf"

// JAWABAN
console.log("\nJAWABAN NOMOR 1")
if (nama == "" && peran == "") {
    console.log("Nama harus diisi!")
} else if (nama == "John" && peran == "") {
    console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!")
} else if (nama == "Jane" && peran == "Penyihir") {
    console.log("Selamat datang di Dunia Werewolf, " + nama)
    console.log("Halo " + peran + " " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!")
} else if (nama == "Jenita" && peran == "Guard") {
    console.log("Selamat datang di Dunia Werewolf, " + nama)
    console.log("Halo " + peran + " " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.")
} else if (nama == "Junaedi" && peran == "Werewolf") {
    console.log("Selamat datang di Dunia Werewolf, " + nama)
    console.log("Halo " + peran + " " + nama + ", Kamu akan memakan mangsa setiap malam!")
}


// SOAL NOMOR 2
var tanggal = 2;
var bulan = 3;
var tahun = 1930;

var bulanString;

switch (bulan) {
    case 1:
        bulanString = "Januari"
        break;
    case 2:
        bulanString = "Februari"
        break;
    case 3:
        bulanString = "Maret"
        break;
    case 4:
        bulanString = "April"
        break;
    case 5:
        bulanString = "Mei"
        break;
    case 6:
        bulanString = "Juni"
        break;
    case 7:
        bulanString = "Juli"
        break;
    case 8:
        bulanString = "Agustus"
        break;
    case 9:
        bulanString = "September"
        break;
    case 10:
        bulanString = "Oktober"
        break;
    case 11:
        bulanString = "November"
        break;
    case 12:
        bulanString = "Desember"
        break;
}
console.log("\nJAWABAN NOMOR 2")
console.log(tanggal+" "+bulanString+" "+tahun)