// Nomor 1 Fungsi Teriak
function teriak() {
    return "Halo Sanbers!"
}

console.log("\nNomor 1\n" + teriak())

// Nomor 2 Fungsi Kalikan
function kalikan(x, y) {
    return x * y
}

var num1 = 12
var num2 = 4
var hasilKali = kalikan(num1, num2)
console.log("\nNomor 2\n" + hasilKali)

// Nomor 3 Fungsi Introduction
function introduce(a, b, c, d) {
    return "Nama saya " + a + ", umur saya " + b + " tahun, alamat saya di " + c + ", dan saya punya hobby yaitu " + d + "!"
}
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log("\nNomor 3\n" +perkenalan) 