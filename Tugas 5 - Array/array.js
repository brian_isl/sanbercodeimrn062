// SOAL NOMOR 1
function range(start, end) {
    var array = [];
    if (end > start) {
        for (let i = start; i <= end; i++) {
            array.push(i);
        }
    } else if (start > end) {
        for (let j = start; j >= end; j--) {
            array.push(j)
        }
    } else if ((start == null && end == null) || (start == null) || (end == null)) {
        array.push(-1)
    }
    return array;
}
console.log("\nJawaban No 1")
console.log(range(1, 10))
console.log(range(1))
console.log(range(11, 18))
console.log(range(54, 50))
console.log(range())

// SOAL NOMOR 2
function rangeWithStep(startNum, finishNum, step) {
    var array = []
    if (finishNum > startNum) {
        for (let i = startNum; i <= finishNum; i += step) {
            array.push(i);
        }
    } else if (startNum > finishNum) {
        for (let j = startNum; j >= finishNum; j -= step) {
            array.push(j)
        }
    }
    return array
}
console.log("\nJawaban No 2")
console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))

// SOAL NOMOR 3
function sum(startNum = 0, finishNum = 0, step = 1) {
    var array = []
    var jumlah = 0
    if (finishNum > startNum) {
        for (let i = startNum; i <= finishNum; i += step) {
            array.push(i);
        }
    } else if (startNum > finishNum) {
        for (let j = startNum; j >= finishNum; j -= step) {
            array.push(j)
        }
    }

    for (let k = 0; k < array.length; k++) {
        jumlah = jumlah + array[k]
    }
    return jumlah
}
console.log("\nJawaban No 3")
console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) //0

// SOAL NOMOR 4
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

function dataHandling(arrayInput) {
    var pesan = ""
    for (var i = 0; i < arrayInput.length; i++) {
        for (var j = 0; j < arrayInput[i].length; j++) {
            if (j == 0) {
                pesan = pesan + "Nomor ID: " + arrayInput[i][0] + "\n"
            } else if (j == 1) {
                pesan = pesan + "Nama Lengkap: " + arrayInput[i][1] + "\n"
            } else if (j == 2) {
                pesan = pesan + "TTL: " + arrayInput[i][2] + " " + arrayInput[i][3] + "\n"
            } else if (j == 3) {

            } else if (j == 4) {
                pesan = pesan + "Hobi: " + arrayInput[i][4] + "\n"
            }
        }
        pesan = pesan + "\n"
    }
    return pesan
}
console.log("\nJawaban Nomor 4")
console.log(dataHandling(input))

// SOAL NOMOR 5
function balikKata(x) {
    var y = "";

    for (var i = x.length - 1; i >= 0; i--) {
        y += x[i];

    }
    return y;
}

console.log("Jawaban Nomor 5")
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

// SOAL NOMOR 6
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];

function dataHandling2(arrayInput) {
    var newArray = arrayInput
    newArray.splice(4, 1, "Pria", "SMA Internasional Metro")
    console.log(newArray)
    var newSplit
    for (var i = 0; i < newArray.length; i++) {
        if (i == 3) {
            newSplit = newArray[3].split("/")
            if (newSplit[1] == "01") {
                console.log(newSplit[0] + "-Januari-" + newSplit[2])
            } else if (newSplit[1] == "02") {
                console.log(newSplit[0] + "-Februari-" + newSplit[2])
            } 
            else if (newSplit[1] == "03") {
                console.log(newSplit[0] + "-Maret-" + newSplit[2])
            }
            else if (newSplit[1] == "04") {
                console.log(newSplit[0] + "-April-" + newSplit[2])
            }
            else if (newSplit[1] == "05") {
                console.log(newSplit[0] + "-Mei-" + newSplit[2])
            }
            else if (newSplit[1] == "06") {
                console.log(newSplit[0] + "-Juni-" + newSplit[2])
            }
            else if (newSplit[1] == "07") {
                console.log(newSplit[0] + "-Juli-" + newSplit[2])
            }
            else if (newSplit[1] == "08") {
                console.log(newSplit[0] + "-Agustus-" + newSplit[2])
            }
            else if (newSplit[1] == "09") {
                console.log(newSplit[0] + "-September-" + newSplit[2])
            }
            else if (newSplit[1] == "10") {
                console.log(newSplit[0] + "-Oktober-" + newSplit[2])
            }
            else if (newSplit[1] == "11") {
                console.log(newSplit[0] + "-November-" + newSplit[2])
            }
            else if (newSplit[1] == "12") {
                console.log(newSplit[0] + "-Desember-" + newSplit[2])
            }

        } else {
            console.log(newArray[i])
        }
    }

}
console.log("\nJawaban Nomor 6")
dataHandling2(input);

/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */